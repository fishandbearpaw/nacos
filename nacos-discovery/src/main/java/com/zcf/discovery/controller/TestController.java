package com.zcf.discovery.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.zcf.discovery.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhaochaofeng
 * @date 2020/7/20 15:56
 */
@RestController
@RefreshScope
public class TestController {

    @Value(value = "${service.name}")
    private String serverName;

    @Value(value = "${service.test1}")
    private String serverTest1;

    @Value(value = "${service.test2}")
    private String serverTest2;

    @Autowired
    private TestService testService;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(@RequestParam String name) {
        return testService.test(serverTest2+", "+serverTest1+", "+serverName+", "+name);
    }
}
