package com.zcf.discovery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhaochaofeng
 * @date 2020/7/26 17:41
 */
@RestController
public class EchoController {

    private final RestTemplate restTemplate;

    @Autowired
    public EchoController(RestTemplate restTemplate) {this.restTemplate = restTemplate;}

    @RequestMapping(value = "/echo", method = RequestMethod.GET)
    public String echo(@RequestParam String name) {
        return restTemplate.getForObject("http://service-provider/test?name=" + name, String.class);
    }

}
