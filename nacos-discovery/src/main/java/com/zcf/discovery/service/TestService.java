package com.zcf.discovery.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * @author zhaochaofeng
 * @date 2020/7/24 15:47
 */
@Service
public class TestService {

    @SentinelResource(value = "test")
    public String test(String name) {
        return "Hello Nacos Discovery " + name;
    }

}
