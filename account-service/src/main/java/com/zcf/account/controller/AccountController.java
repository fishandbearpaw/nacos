package com.zcf.account.controller;

import cn.hutool.http.HttpUtil;
import com.baomidou.mybatisplus.extension.api.R;
import com.zcf.account.service.AccountService;
import io.seata.core.context.RootContext;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/15 13:58
 */
@Log4j2
@RestController
@RequestMapping(value="/account")
public class AccountController {

    private Random random = new Random();
    @Autowired
    private AccountService accountService;

    /**
     * 消费money
     * @param userId
     * @param money
     * @return
     */
    @PostMapping(value = "/consumption", produces = "application/json")
    public String consumption(String userId, int money) {
        log.info("Account Service ... xid: " + RootContext.getXID());
//        if (random.nextBoolean()) {
//            throw new RuntimeException("this-is a mock Exception");
//        }
        boolean flag = accountService.consumption(userId, money);
        log.info("Account Service End ... ");
        if (flag) {
            return "SUCCESS";
        }
        return "FAIL";
    }
}
