package com.zcf.account.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zcf.entity.service3.major.entity.TAccountTbl;
import com.zcf.entity.service3.major.service.TAccountTblService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/15 14:02
 */
@Service
public class AccountService {

    @Autowired
    private TAccountTblService accountTblService;

    @SentinelResource(value = "consumption")
    public boolean consumption(String userId, int money) {
        QueryWrapper<TAccountTbl> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(TAccountTbl.Columns.userId, userId);
        List<TAccountTbl> accountTblList = accountTblService.list(queryWrapper);
        TAccountTbl accountTbl = null;
        if(accountTblList.size()>0){
            accountTbl = accountTblList.get(0);
        }else{
            return false;
        }
        accountTbl.setMoney(accountTbl.getMoney().intValue() - money);
        return accountTblService.updateById(accountTbl);
    }

}
