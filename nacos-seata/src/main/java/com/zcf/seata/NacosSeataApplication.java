package com.zcf.seata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zhaochaofeng
 * @date 2020/7/22 14:56
 */
@ComponentScan(basePackages = {"com.zcf.seata", "com.zcf.entity.center"})
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient(autoRegister = false)
public class NacosSeataApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosSeataApplication.class, args);
    }
}
