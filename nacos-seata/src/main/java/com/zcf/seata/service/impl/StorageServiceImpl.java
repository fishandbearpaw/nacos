package com.zcf.seata.service.impl;

import com.zcf.seata.service.StorageService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/17 16:02
 */
@Log4j2
@Component
public class StorageServiceImpl implements StorageService {

    @Override
    public String storage(String commodityCode, int count) {
        log.info("storage service 熔断了");
        return null;
    }
}
