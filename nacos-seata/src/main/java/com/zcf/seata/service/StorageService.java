package com.zcf.seata.service;

import com.zcf.seata.service.impl.OrderServiceImpl;
import com.zcf.seata.service.impl.StorageServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/17 16:01
 */
@FeignClient(name = "storage-service", fallback = StorageServiceImpl.class)
public interface StorageService {

    @GetMapping(path = "/storage/reduce/{commodityCode}/{count}")
    String storage(@PathVariable("commodityCode") String commodityCode,
                   @PathVariable("count") int count);

}
