package com.zcf.seata.service;

import com.zcf.seata.service.impl.OrderServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/2 11:47
 */
@FeignClient(name = "order-service", fallback = OrderServiceImpl.class)
public interface OrderService {

    @PostMapping(path = "/order")
    String order(@RequestParam("userId") String userId,
                 @RequestParam("commodityCode") String commodityCode,
                 @RequestParam("orderCount") int orderCount);

}
