package com.zcf.seata.service.impl;

import com.zcf.seata.service.OrderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/2 11:46
 */
@Log4j2
@Component
public class OrderServiceImpl implements OrderService {

    @Override
    public String order(String userId, String commodityCode, int orderCount) {
        log.info("order service 熔断了");
        return null;
    }
}
