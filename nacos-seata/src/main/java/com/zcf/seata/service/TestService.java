package com.zcf.seata.service;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/21 13:23
 */
public interface TestService {

    public void test();

}
