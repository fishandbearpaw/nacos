package com.zcf.seata.service.impl;

import com.zcf.seata.service.OrderService;
import com.zcf.seata.service.StorageService;
import com.zcf.seata.service.TestService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/21 13:13
 */
@Service
public class TestServiceImpl implements TestService {

    private static final String SUCCESS = "SUCCESS";

    private static final String USER_ID = "U100001";

    private static final String COMMODITY_CODE = "C00321";

    private static final int ORDER_COUNT = 2;

    @Resource
    private OrderService orderService;

    @Resource
    private StorageService storageService;

    @Override
    @GlobalTransactional(timeoutMills = 6000, name = "spring-cloud-demo-tx")
    @Transactional(rollbackFor = Exception.class)
    public void test(){
        String result = storageService.storage(COMMODITY_CODE, ORDER_COUNT);

        if (!SUCCESS.equals(result)) {
            throw new RuntimeException("执行失败");
        }


//        result = orderService.order(USER_ID, COMMODITY_CODE, ORDER_COUNT);
//
//        if (!SUCCESS.equals(result)) {
//            throw new RuntimeException("执行失败");
//        }
    }

}
