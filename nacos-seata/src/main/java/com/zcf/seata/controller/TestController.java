package com.zcf.seata.controller;

import com.zcf.seata.service.OrderService;
import com.zcf.seata.service.StorageService;
import com.zcf.seata.service.impl.TestServiceImpl;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 测试seata功能
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/2 11:07
 */
@RestController
public class TestController {

    private static final String SUCCESS = "SUCCESS";

    @Resource
    private TestServiceImpl testService;

    @GetMapping(value = "/seata/rest", produces = "application/json")
    public String rest() {
        testService.test();
        return SUCCESS;
    }

}
