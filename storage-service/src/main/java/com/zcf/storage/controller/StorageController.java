package com.zcf.storage.controller;

import com.zcf.storage.service.StorageService;
import io.seata.core.context.RootContext;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/15 16:58
 */
@Log4j2
@RestController
@RequestMapping(value="/storage")
public class StorageController {

    @Autowired
    private StorageService storageService;

    @GetMapping(value = "reduce/{commodityCode}/{count}")
    public String reduce(@PathVariable String commodityCode, @PathVariable int count) {
        log.info("Storage Service Begin ... xid: " + RootContext.getXID());
        boolean flag = storageService.reduce(commodityCode, count);
        log.info("Storage Service End ... ");
        if (flag) {
            return "SUCCESS";
        }
        return "FAIL";
    }

}
