package com.zcf.storage.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zcf.entity.service1.major.entity.TStorageTbl;
import com.zcf.entity.service1.major.service.TStorageTblService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/15 17:04
 */
@Service
public class StorageService {

    @Autowired
    private TStorageTblService storageTblService;

    @SentinelResource(value = "reduce")
    public boolean reduce(String commodityCode, int count) {
        QueryWrapper<TStorageTbl> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(TStorageTbl.Columns.commodityCode, commodityCode);
        List<TStorageTbl> storageTblList = storageTblService.list(queryWrapper);
        TStorageTbl storageTbl = null;
        if(!storageTblList.isEmpty()){
            storageTbl = storageTblList.get(0);
        }else{
            return false;
        }
        storageTbl.setCount(storageTbl.getCount().intValue() - count);
        return storageTblService.updateById(storageTbl);
    }

}
