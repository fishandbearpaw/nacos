package com.zcf.seata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zhaochaofeng
 * @date 2020/7/22 14:56
 */
@ComponentScan(basePackages = {"com.zcf"})
@SpringBootApplication
public class NacosGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosGatewayApplication.class, args);
    }
}
