## 结构说明

- nacos-discovery 注册服务
- nacos-feign 调用服务
- nacos-gateway 网关

## 环境说明
- nacos控制台使用的是1.3.0版本
- sentinel控制台使用的是1.7.2版本

# nacos-discovery
包含discovery，config，sentinel，zipkin，sleuth的基础功能

<font color=#ff0000 size=3 face="黑体">注：如果你在使用config的同时，还使用了sentinel-datasource-nacos包，需剔除nacos-client</font>


# sentinel
基于1.7.2版本，实现了规则的持久化，逻辑如下图所示
![ecosystem-landscape](./image/1.png)
具体流程，参考https://my.oschina.net/zyjason91/blog/3077872
当前仅有控流规则，后续可自行实现