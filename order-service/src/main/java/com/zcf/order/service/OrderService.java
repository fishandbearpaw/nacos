package com.zcf.order.service;

import com.zcf.entity.service2.major.entity.TOrderTbl;
import com.zcf.entity.service2.major.service.TOrderTblService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/16 11:30
 */
@Service
public class OrderService {

    @Autowired
    private TOrderTblService orderTblService;

    /**
     * 插入订单
     * @param orderTbl
     * @return
     */
    public boolean addOrder(TOrderTbl orderTbl) {
        return orderTblService.save(orderTbl);
    }

}
