package com.zcf.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/16 14:22
 */
@FeignClient(name="account-service" , fallback = AccountSentinel.class)
public interface AccountClient {

    @PostMapping("/account/consumption")
    String consumption(@RequestParam(name="userId") String userId, @RequestParam(name="money") int money);

}
