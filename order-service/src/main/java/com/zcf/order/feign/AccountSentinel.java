package com.zcf.order.feign;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhaochaofeng
 * @version 1.0
 * @date 2020/9/16 14:25
 */
@Log4j2
@Component
public class AccountSentinel implements AccountClient {

    @Override
    public String consumption(String userId, int money) {
        log.info("account-service consumption接口熔断了！");
        return "FAIL";
    }
}
