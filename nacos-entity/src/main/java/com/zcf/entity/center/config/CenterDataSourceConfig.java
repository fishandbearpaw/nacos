package com.zcf.entity.center.config;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaochaofeng
 * @date 2020/7/31 10:23
 */
@Configuration
@MapperScan(basePackages = {"com.zcf.entity.center.major.mapper", "com.zcf.entity.center.custom.mapper"}, sqlSessionFactoryRef = "centerSqlSessionFactory")
public class CenterDataSourceConfig {

    static final String MAPPER_LOCATION = "classpath*:com/zcf/entity/center/*/mapper/xml/*.xml";

    @Bean(name = "centerDataSource")
    @Primary
    @ConfigurationProperties(prefix="center.datasource")
    public DataSource dataSource() throws SQLException {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setFilters("wall");
        //开启配置multiStatementAllow支持一次执行多条sql
        WallConfig wallConfig = new WallConfig();
        wallConfig.setMultiStatementAllow(Boolean.TRUE);
        WallFilter wallFilter = new WallFilter();
        wallFilter.setDbType("mysql");
        wallFilter.setConfig(wallConfig);
        List list = new ArrayList<Filter>();
        list.add(wallFilter);
        dataSource.setProxyFilters(list);
        //结束
        return dataSource;
    }

    @Bean(name = "centerTransactionManager")
    @Primary
    public DataSourceTransactionManager transactionManager(@Qualifier("centerDataSource") DataSource centerDataSource) {
        return new DataSourceTransactionManager(centerDataSource);
    }

    @Bean(name = "centerSqlSessionFactory")
    @Primary
    public SqlSessionFactory sqlSessionFactory(@Qualifier("centerDataSource") DataSource cnecDataSource)
            throws Exception {
        final MybatisSqlSessionFactoryBean sessionFactory = new MybatisSqlSessionFactoryBean();
        sessionFactory.setDataSource(cnecDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(CenterDataSourceConfig.MAPPER_LOCATION));
        Interceptor[] interceptor = {pageInterceptor()};
        sessionFactory.setPlugins(interceptor);
        return sessionFactory.getObject();
    }

    @Bean(name = "centerPageInterceptor")
    @Primary
    public PaginationInterceptor pageInterceptor() {
        PaginationInterceptor pageInterceptor = new PaginationInterceptor();
        pageInterceptor.setDialectType(DbType.MYSQL.getDb());
        return pageInterceptor;
    }
}
