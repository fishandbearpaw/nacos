package com.zcf.entity.center.major.service;

import com.zcf.entity.center.major.entity.UndoLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-09-02
 */
public interface UndoLogService extends IService<UndoLog> {

}
