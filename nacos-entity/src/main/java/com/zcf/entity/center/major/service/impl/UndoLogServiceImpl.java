package com.zcf.entity.center.major.service.impl;

import com.zcf.entity.center.major.entity.UndoLog;
import com.zcf.entity.center.major.mapper.UndoLogMapper;
import com.zcf.entity.center.major.service.UndoLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-09-02
 */
@Service
public class UndoLogServiceImpl extends ServiceImpl<UndoLogMapper, UndoLog> implements UndoLogService {

}
