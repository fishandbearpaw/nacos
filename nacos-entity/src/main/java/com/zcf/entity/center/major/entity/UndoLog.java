package com.zcf.entity.center.major.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-09-02
 */
public class UndoLog extends Model<UndoLog> {

    /**
    * 所有字段名
    */
    public static class Columns {
        /**
        * 
        */
        public static final String id = "id";
        /**
        * 
        */
        public static final String branchId = "branch_id";
        /**
        * 
        */
        public static final String xid = "xid";
        /**
        * 
        */
        public static final String context = "context";
        /**
        * 
        */
        public static final String rollbackInfo = "rollback_info";
        /**
        * 
        */
        public static final String logStatus = "log_status";
        /**
        * 
        */
        public static final String logCreated = "log_created";
        /**
        * 
        */
        public static final String logModified = "log_modified";
        /**
        * 
        */
        public static final String ext = "ext";
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("branch_id")
    private Long branchId;

    private String xid;

    private String context;

    @TableField("rollback_info")
    private byte[] rollbackInfo;

    @TableField("log_status")
    private Integer logStatus;

    @TableField("log_created")
    private LocalDateTime logCreated;

    @TableField("log_modified")
    private LocalDateTime logModified;

    private String ext;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public byte[] getRollbackInfo() {
        return rollbackInfo;
    }

    public void setRollbackInfo(byte[] rollbackInfo) {
        this.rollbackInfo = rollbackInfo;
    }

    public Integer getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Integer logStatus) {
        this.logStatus = logStatus;
    }

    public LocalDateTime getLogCreated() {
        return logCreated;
    }

    public void setLogCreated(LocalDateTime logCreated) {
        this.logCreated = logCreated;
    }

    public LocalDateTime getLogModified() {
        return logModified;
    }

    public void setLogModified(LocalDateTime logModified) {
        this.logModified = logModified;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "UndoLog{" +
        "id=" + id +
        ", branchId=" + branchId +
        ", xid=" + xid +
        ", context=" + context +
        ", rollbackInfo=" + rollbackInfo +
        ", logStatus=" + logStatus +
        ", logCreated=" + logCreated +
        ", logModified=" + logModified +
        ", ext=" + ext +
        "}";
    }
}
