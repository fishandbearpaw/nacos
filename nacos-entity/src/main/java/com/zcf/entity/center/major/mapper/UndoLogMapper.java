package com.zcf.entity.center.major.mapper;

import com.zcf.entity.center.major.entity.UndoLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-09-02
 */
public interface UndoLogMapper extends BaseMapper<UndoLog> {

}
