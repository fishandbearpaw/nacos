package com.zcf.entity.center.major.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * , 查询对象
 * </p>
 *
 * @author ${author}
 * @since 2020-09-02
 */
public class UndoLogSearch implements Serializable {

    @TableField("id")
    private Long id;

    /**
     *  - 开始
     */
    @TableField("id")
    private Long idStart;

    /**
     *  - 结束
     */
    @TableField("id")
    private Long idEnd;
    @TableField("branch_id")
    private Long branchId;

    /**
     *  - 开始
     */
    @TableField("branch_id")
    private Long branchIdStart;

    /**
     *  - 结束
     */
    @TableField("branch_id")
    private Long branchIdEnd;
    @TableField("xid")
    private String xid;

    @TableField("context")
    private String context;

    @TableField("rollback_info")
    private byte[] rollbackInfo;

    @TableField("log_status")
    private Integer logStatus;

    /**
     *  - 开始
     */
    @TableField("log_status")
    private Integer logStatusStart;

    /**
     *  - 结束
     */
    @TableField("log_status")
    private Integer logStatusEnd;
    @TableField("log_created")
    private LocalDateTime logCreated;

    /**
     *  - 开始
     */
    @TableField("log_created")
    private LocalDateTime logCreatedStart;

    /**
     *  - 结束
     */
    @TableField("log_created")
    private LocalDateTime logCreatedEnd;
    @TableField("log_modified")
    private LocalDateTime logModified;

    /**
     *  - 开始
     */
    @TableField("log_modified")
    private LocalDateTime logModifiedStart;

    /**
     *  - 结束
     */
    @TableField("log_modified")
    private LocalDateTime logModifiedEnd;
    @TableField("ext")
    private String ext;


    /**
     * 获取: 
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置: 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取:  - 开始
     */
    public Long getIdStart() {
        return idStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setIdStart(Long id) {
        this.idStart = id;
    }

    /**
     * 获取:  - 结束
     */
    public Long getIdEnd() {
        return idEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setIdEnd(Long id) {
        this.idEnd = id;
        }
    /**
     * 获取: 
     */
    public Long getBranchId() {
        return branchId;
    }

    /**
     * 设置: 
     */
    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    /**
     * 获取:  - 开始
     */
    public Long getBranchIdStart() {
        return branchIdStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setBranchIdStart(Long branchId) {
        this.branchIdStart = branchId;
    }

    /**
     * 获取:  - 结束
     */
    public Long getBranchIdEnd() {
        return branchIdEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setBranchIdEnd(Long branchId) {
        this.branchIdEnd = branchId;
        }
    /**
     * 获取: 
     */
    public String getXid() {
        return xid;
    }

    /**
     * 设置: 
     */
    public void setXid(String xid) {
        this.xid = xid;
    }

    /**
     * 获取: 
     */
    public String getContext() {
        return context;
    }

    /**
     * 设置: 
     */
    public void setContext(String context) {
        this.context = context;
    }

    /**
     * 获取: 
     */
    public byte[] getRollbackInfo() {
        return rollbackInfo;
    }

    /**
     * 设置: 
     */
    public void setRollbackInfo(byte[] rollbackInfo) {
        this.rollbackInfo = rollbackInfo;
    }

    /**
     * 获取: 
     */
    public Integer getLogStatus() {
        return logStatus;
    }

    /**
     * 设置: 
     */
    public void setLogStatus(Integer logStatus) {
        this.logStatus = logStatus;
    }

    /**
     * 获取:  - 开始
     */
    public Integer getLogStatusStart() {
        return logStatusStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setLogStatusStart(Integer logStatus) {
        this.logStatusStart = logStatus;
    }

    /**
     * 获取:  - 结束
     */
    public Integer getLogStatusEnd() {
        return logStatusEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setLogStatusEnd(Integer logStatus) {
        this.logStatusEnd = logStatus;
        }
    /**
     * 获取: 
     */
    public LocalDateTime getLogCreated() {
        return logCreated;
    }

    /**
     * 设置: 
     */
    public void setLogCreated(LocalDateTime logCreated) {
        this.logCreated = logCreated;
    }

    /**
     * 获取:  - 开始
     */
    public LocalDateTime getLogCreatedStart() {
        return logCreatedStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setLogCreatedStart(LocalDateTime logCreated) {
        this.logCreatedStart = logCreated;
    }

    /**
     * 获取:  - 结束
     */
    public LocalDateTime getLogCreatedEnd() {
        return logCreatedEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setLogCreatedEnd(LocalDateTime logCreated) {
        this.logCreatedEnd = logCreated;
        }
    /**
     * 获取: 
     */
    public LocalDateTime getLogModified() {
        return logModified;
    }

    /**
     * 设置: 
     */
    public void setLogModified(LocalDateTime logModified) {
        this.logModified = logModified;
    }

    /**
     * 获取:  - 开始
     */
    public LocalDateTime getLogModifiedStart() {
        return logModifiedStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setLogModifiedStart(LocalDateTime logModified) {
        this.logModifiedStart = logModified;
    }

    /**
     * 获取:  - 结束
     */
    public LocalDateTime getLogModifiedEnd() {
        return logModifiedEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setLogModifiedEnd(LocalDateTime logModified) {
        this.logModifiedEnd = logModified;
        }
    /**
     * 获取: 
     */
    public String getExt() {
        return ext;
    }

    /**
     * 设置: 
     */
    public void setExt(String ext) {
        this.ext = ext;
    }

}