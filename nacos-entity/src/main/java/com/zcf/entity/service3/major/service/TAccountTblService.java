package com.zcf.entity.service3.major.service;

import com.zcf.entity.service3.major.entity.TAccountTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public interface TAccountTblService extends IService<TAccountTbl> {

}
