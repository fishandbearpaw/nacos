package com.zcf.entity.service3.major.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public class TAccountTbl extends Model<TAccountTbl> {

    /**
    * 所有字段名
    */
    public static class Columns {
        /**
        * 
        */
        public static final String id = "id";
        /**
        * 
        */
        public static final String userId = "user_id";
        /**
        * 
        */
        public static final String money = "money";
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("user_id")
    private String userId;

    private Integer money;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TAccountTbl{" +
        "id=" + id +
        ", userId=" + userId +
        ", money=" + money +
        "}";
    }
}
