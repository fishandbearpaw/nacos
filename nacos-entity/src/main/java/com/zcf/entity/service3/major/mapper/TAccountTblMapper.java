package com.zcf.entity.service3.major.mapper;

import com.zcf.entity.service3.major.entity.TAccountTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public interface TAccountTblMapper extends BaseMapper<TAccountTbl> {

}
