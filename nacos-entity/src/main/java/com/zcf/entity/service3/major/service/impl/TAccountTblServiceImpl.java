package com.zcf.entity.service3.major.service.impl;

import com.zcf.entity.service3.major.entity.TAccountTbl;
import com.zcf.entity.service3.major.mapper.TAccountTblMapper;
import com.zcf.entity.service3.major.service.TAccountTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
@Service
public class TAccountTblServiceImpl extends ServiceImpl<TAccountTblMapper, TAccountTbl> implements TAccountTblService {

}
