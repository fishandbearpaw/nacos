package com.zcf.entity.service3.major.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * , 查询对象
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public class TAccountTblSearch implements Serializable {

    @TableField("id")
    private Integer id;

    /**
     *  - 开始
     */
    @TableField("id")
    private Integer idStart;

    /**
     *  - 结束
     */
    @TableField("id")
    private Integer idEnd;
    @TableField("user_id")
    private String userId;

    @TableField("money")
    private Integer money;

    /**
     *  - 开始
     */
    @TableField("money")
    private Integer moneyStart;

    /**
     *  - 结束
     */
    @TableField("money")
    private Integer moneyEnd;

    /**
     * 获取: 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置: 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取:  - 开始
     */
    public Integer getIdStart() {
        return idStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setIdStart(Integer id) {
        this.idStart = id;
    }

    /**
     * 获取:  - 结束
     */
    public Integer getIdEnd() {
        return idEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setIdEnd(Integer id) {
        this.idEnd = id;
        }
    /**
     * 获取: 
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置: 
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取: 
     */
    public Integer getMoney() {
        return money;
    }

    /**
     * 设置: 
     */
    public void setMoney(Integer money) {
        this.money = money;
    }

    /**
     * 获取:  - 开始
     */
    public Integer getMoneyStart() {
        return moneyStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setMoneyStart(Integer money) {
        this.moneyStart = money;
    }

    /**
     * 获取:  - 结束
     */
    public Integer getMoneyEnd() {
        return moneyEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setMoneyEnd(Integer money) {
        this.moneyEnd = money;
        }
}