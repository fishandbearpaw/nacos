package com.zcf.entity.service1.major.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public class TStorageTbl extends Model<TStorageTbl> {

    /**
    * 所有字段名
    */
    public static class Columns {
        /**
        * 
        */
        public static final String id = "id";
        /**
        * 
        */
        public static final String commodityCode = "commodity_code";
        /**
        * 
        */
        public static final String count = "count";
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("commodity_code")
    private String commodityCode;

    private Integer count;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TStorageTbl{" +
        "id=" + id +
        ", commodityCode=" + commodityCode +
        ", count=" + count +
        "}";
    }
}
