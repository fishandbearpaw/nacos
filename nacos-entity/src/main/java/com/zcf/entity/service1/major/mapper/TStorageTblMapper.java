package com.zcf.entity.service1.major.mapper;

import com.zcf.entity.service1.major.entity.TStorageTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public interface TStorageTblMapper extends BaseMapper<TStorageTbl> {

}
