package com.zcf.entity.service1.major.service;

import com.zcf.entity.service1.major.entity.TStorageTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public interface TStorageTblService extends IService<TStorageTbl> {

}
