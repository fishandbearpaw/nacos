package com.zcf.entity.service1.major.service.impl;

import com.zcf.entity.service1.major.entity.TStorageTbl;
import com.zcf.entity.service1.major.mapper.TStorageTblMapper;
import com.zcf.entity.service1.major.service.TStorageTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
@Service
public class TStorageTblServiceImpl extends ServiceImpl<TStorageTblMapper, TStorageTbl> implements TStorageTblService {

}
