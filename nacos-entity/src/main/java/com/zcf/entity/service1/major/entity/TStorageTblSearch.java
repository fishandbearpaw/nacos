package com.zcf.entity.service1.major.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * , 查询对象
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public class TStorageTblSearch implements Serializable {

    @TableField("id")
    private Integer id;

    /**
     *  - 开始
     */
    @TableField("id")
    private Integer idStart;

    /**
     *  - 结束
     */
    @TableField("id")
    private Integer idEnd;
    @TableField("commodity_code")
    private String commodityCode;

    @TableField("count")
    private Integer count;

    /**
     *  - 开始
     */
    @TableField("count")
    private Integer countStart;

    /**
     *  - 结束
     */
    @TableField("count")
    private Integer countEnd;

    /**
     * 获取: 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置: 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取:  - 开始
     */
    public Integer getIdStart() {
        return idStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setIdStart(Integer id) {
        this.idStart = id;
    }

    /**
     * 获取:  - 结束
     */
    public Integer getIdEnd() {
        return idEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setIdEnd(Integer id) {
        this.idEnd = id;
        }
    /**
     * 获取: 
     */
    public String getCommodityCode() {
        return commodityCode;
    }

    /**
     * 设置: 
     */
    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    /**
     * 获取: 
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 设置: 
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 获取:  - 开始
     */
    public Integer getCountStart() {
        return countStart;
    }

    /**
     * 设置:  - 开始
     */
    public void setCountStart(Integer count) {
        this.countStart = count;
    }

    /**
     * 获取:  - 结束
     */
    public Integer getCountEnd() {
        return countEnd;
    }

    /**
     * 设置:  - 结束
     */
    public void setCountEnd(Integer count) {
        this.countEnd = count;
        }
}