package com.zcf.entity.service2.major.service;

import com.zcf.entity.service2.major.entity.TOrderTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public interface TOrderTblService extends IService<TOrderTbl> {

}
