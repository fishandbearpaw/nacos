package com.zcf.entity.service2.config;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaochaofeng
 * @date 2020/7/31 10:23
 */
@Configuration
@MapperScan(basePackages = {"com.zcf.entity.service2.major.mapper", "com.zcf.entity.service2.custom.mapper"}, sqlSessionFactoryRef = "service2SqlSessionFactory")
public class Service2DataSourceConfig {

    static final String MAPPER_LOCATION = "classpath*:com/zcf/entity/service2/*/mapper/xml/*.xml";

    @Bean(name = "service2DataSource")
    @ConfigurationProperties(prefix="service2.datasource")
    public DataSource dataSource() throws SQLException {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setFilters("wall");
        //开启配置multiStatementAllow支持一次执行多条sql
        WallConfig wallConfig = new WallConfig();
        wallConfig.setMultiStatementAllow(Boolean.TRUE);
        WallFilter wallFilter = new WallFilter();
        wallFilter.setDbType("mysql");
        wallFilter.setConfig(wallConfig);
        List list = new ArrayList<Filter>();
        list.add(wallFilter);
        dataSource.setProxyFilters(list);
        //结束
        return dataSource;
    }

    @Bean(name = "service2TransactionManager")
    public DataSourceTransactionManager transactionManager(@Qualifier("service2DataSource") DataSource service2DataSource) {
        return new DataSourceTransactionManager(service2DataSource);
    }

    @Bean(name = "service2SqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory(@Qualifier("service2DataSource") DataSource service2DataSource)
            throws Exception {
        final MybatisSqlSessionFactoryBean sessionFactory = new MybatisSqlSessionFactoryBean();
        sessionFactory.setDataSource(service2DataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(Service2DataSourceConfig.MAPPER_LOCATION));
        Interceptor[] interceptor = {pageInterceptor()};
        sessionFactory.setPlugins(interceptor);
        return sessionFactory.getObject();
    }

    @Bean(name = "service2PageInterceptor")
    public PaginationInterceptor pageInterceptor() {
        PaginationInterceptor pageInterceptor = new PaginationInterceptor();
        pageInterceptor.setDialectType(DbType.MYSQL.getDb());
        return pageInterceptor;
    }
}
