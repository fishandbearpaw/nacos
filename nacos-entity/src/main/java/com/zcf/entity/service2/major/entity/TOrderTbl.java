package com.zcf.entity.service2.major.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public class TOrderTbl extends Model<TOrderTbl> {

    /**
    * 所有字段名
    */
    public static class Columns {
        /**
        * 
        */
        public static final String id = "id";
        /**
        * 
        */
        public static final String userId = "user_id";
        /**
        * 
        */
        public static final String commodityCode = "commodity_code";
        /**
        * 
        */
        public static final String count = "count";
        /**
        * 
        */
        public static final String money = "money";
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("user_id")
    private String userId;

    @TableField("commodity_code")
    private String commodityCode;

    private Integer count;

    private Integer money;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TOrderTbl{" +
        "id=" + id +
        ", userId=" + userId +
        ", commodityCode=" + commodityCode +
        ", count=" + count +
        ", money=" + money +
        "}";
    }
}
