package com.zcf.entity.service2.major.mapper;

import com.zcf.entity.service2.major.entity.TOrderTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
public interface TOrderTblMapper extends BaseMapper<TOrderTbl> {

}
