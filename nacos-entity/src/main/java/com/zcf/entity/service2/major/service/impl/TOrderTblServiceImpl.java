package com.zcf.entity.service2.major.service.impl;

import com.zcf.entity.service2.major.entity.TOrderTbl;
import com.zcf.entity.service2.major.mapper.TOrderTblMapper;
import com.zcf.entity.service2.major.service.TOrderTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-09-17
 */
@Service
public class TOrderTblServiceImpl extends ServiceImpl<TOrderTblMapper, TOrderTbl> implements TOrderTblService {

}
