package com.zcf.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhaochaofeng
 * @date 2020/7/22 15:02
 */
@FeignClient(name="service-provider" , fallback = TestFeignSentinel.class)
public interface TestFeignClient {

    @GetMapping("/test")
    String test(@RequestParam("name") String name);

    @GetMapping("/echo")
    String echo(@RequestParam("name") String name);

}
