package com.zcf.feign.service;

import org.springframework.stereotype.Component;

/**
 * @author zhaochaofeng
 * @date 2020/7/22 15:06
 */
@Component
public class TestFeignSentinel implements TestFeignClient {
    @Override
    public String test(String name) {
        return "test 熔断！";
    }

    @Override
    public String echo(String name) {
        return "echo 熔断！";
    }
}
