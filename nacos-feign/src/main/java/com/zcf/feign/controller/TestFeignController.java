package com.zcf.feign.controller;

import com.zcf.feign.service.TestFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhaochaofeng
 * @date 2020/7/22 15:15
 */
@RestController
public class TestFeignController {

    @Autowired
    private TestFeignClient testFeignClient;

    @RequestMapping("/test/{name}")
    public String test(@PathVariable String name){
        return testFeignClient.test(name);
    }

    @RequestMapping("/echo/{name}")
    public String echo(@PathVariable String name){
        return testFeignClient.echo(name);
    }

}
