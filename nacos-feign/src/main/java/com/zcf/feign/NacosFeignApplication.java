package com.zcf.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhaochaofeng
 * @date 2020/7/22 14:56
 */
@ComponentScan(basePackages = {"com.zcf"})
@SpringBootApplication
@EnableFeignClients
public class NacosFeignApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosFeignApplication.class, args);
    }
}
